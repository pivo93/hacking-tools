# Hacking Tools
Creating famous hacking tools (e.g. network scanners, ARP spoofers) from scratch with Python3.

# Credits
Special thanks to Zaid Sabih from zSecurity and his great course on Udemy "Learn Python & Ethical Hacking from Scratch". All code in this repository emerged from my participation in this course.

# Installing NetfilterQueue
Install `NetfilterQueue` via: `sudo pip3 install -U git+https://github.com/kti/python-netfilterqueue` (`sudo pip3 install` because most python programs in here need to be run with `sudo` rights)

# Installing SSLStrip
1. `git clone https://github.com/moxie0/sslstrip.git`
2. Install `twisted`: `sudo apt-get install python-twisted-web`
3. Change machine into forwarding mode: `sudo bash -c "echo 1 > /proc/sys/net/ipv4/ip_forward"`
4. Create routing from port 80 to port 10000 (which is the port SSLStrip is working on): `sudo iptables -t nat -A PREROUTING -p tcp --destination_port 80 -j REDIRECT --to-port 10000`
5. Become Man-in-the-Middle
6. Run SSLStrip via Python 2 (!)
7. Run e.g. a packet sniffer and just observe the magic (e.g. visit a HTTPS page (via HTTP) in the victim machine -> Make sure, the target page does not use HSTS)

